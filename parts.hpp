#include <string>
#include <iostream>
#ifndef PARTS
#define PARTS

namespace partTypes{
    struct CPU{
        double clock;
        std::string model;
        std::string company;
        bool igpu;
        int price;
        int cores;
        int wattage;
        bool comp;
    };
    struct GPU{
        bool rt;
        std::string model;
        std::string manufacturer;
        int price;
        int wattage;
    }
    struct MOBO{
        int price;
        std::string manufacturer;
        std::string chipset;
        bool comp;
    }
}

struct partsList{
    partTypes::CPU cpu{};
    partTypes::GPU gpu{};
    partTypes::MOBO mobo{};
    int totalPrice{};
    int totalWattage{}
};

namespace parts{
    using namespace partTypes;
    const CPU c0001 {4.4,"Ryzen 5 5600G","AMD",true,219,6,true};

    //GPU

    const GPU g0001{true,"Radeon RX 6500XT","AMD",249};

    //mobo

    const MOBO m0001{129,"ASUS","B550", true};
}


namespace functions{
    partsList createNewList(){
        return partsList{}; //reserved for future
    }
    void printParts(partsList x){
        std::cout<<"Your CPU is:\n"<<x.cpu.company<<' '<<x.cpu.model<<"\nand it costs: "<<x.cpu.price<<(x.cpu.igpu ? " \nYour CPU has an IGPU " : " Your CPU doesn't have an IGPU ")<<"\n The clock speed is: "<<x.cpu.clock<<" x "<<x.cpu.cores<<" cores.\nThe wattage for your CPU is:  "<<x.cpu.wattage<<" \n\n ";

        std::cout<<"Your GPU is:\n"<<x.gpu.manufacturer<<' '<<x.gpu.model<<"\nand it costs: "<<x.gpu.price<<(x.gpu.rt ? "Your GPU supports RayTracing " : "Your GPU doesn't support raytracing")<<"The wattage for your CPU is :"<<x.gpu.wattage<<"\n\n\n";

        std::cout<<"The total wattage of your build is: \n"<<x.totalWattage<<" \n\nThe total price is:\n"<<x.totalPrice<<"\n\n";
    }
    int addToList(partsList& x){
        back:
        char part{};
        std::cout<<"What part? \nc for CPU\ng for GPU\n->";
        std::cin>>part;
        switch (part){
            case 'c':
                int y{};
                std::cout<<"Choose a CPU -> (Enter Serial Number:)\nPress 0 for catalog\n->";
                std::cin>>y;
                switch (y){
                    case 0001: 
                        x.cpu=parts::c0001;
                        x.totalWattage+=parts::c0001.wattage;
                        x.totalPrice+=parts::c0001.price;
                        std::cout<<"AMD Ryzen 5 5600G added sucessfully\n";
                        break;
                    default: 
                        std::cout<<"???";
                        goto back;
                }
                return 0;
            case 'g':
                int y{};
                std::cout<<"Choose a GPU-> (Enter catalog number: )\n press 0 for catalog\n->";
                std::cin>>y;
                switch (y):
                    case 0001:
                        x.gpu=parts::g0001;
                        x.totalWattage+=x.gpu.wattage;
                        x.totalPrice+=x.gpu.price;
                        std::cout<<"AMD RX 6500XT added sucessfully";
                        return 0;
                    case 0:
                        return 0;
                    default:
                        std::cout<<"???";
                        goto back;
            case 'm':
                int y{};
                std::cout<<"Choose a motherboard-> (Enter catalog number)\npress 0 for catalog\n\n->";
                std::cin>>y;
                switch (y){
                    case 0001:
                        x.mobo=parts::m0001;
                        x.totalPrice+=x.mobo.price;
                        std::cout<<" ASUS PRIME B550 added sucessfully";
                        return 0;
                    case 0:
                        return 0;
                    default:
                        goto back;
                }
        }
    }
}


#endif
