#include "parts.hpp"
#include <iostream>

int main(){
    menu:
    char main_menu{};
    std::cout<<"Welcome to PC Builder 2.0\nFor a guide, enter g\nto import a parts list, press i\nto create a blank parts list, press c\n->";
    std::cin>>main_menu;
    partsList LIST;
    switch (main_menu){
        case 'g': 
            std::cout<<"WIP\n";
            goto menu;
        case 'i':
            std::cout<<"WIP\n";
            goto menu;
        case 'c':
            std::cout<<"Creating new list..\n\n";
            LIST = functions::createNewList();
            goto build_menu;
        default: 
            std::cout<<"Invalid!\n";
            goto menu;
    }   

    build_menu:
    char build_menu{};
    std::cout<<"\n\nWelcome to the build menu , press s to see your current build progress\npress a to add a new part\npress x to export your list\n\n you can press b to go back or press q to quit any time.\n->";
    std::cin>>build_menu;
    switch (build_menu){
        case 's': functions::printParts(LIST);goto build_menu;
        case 'a': functions::addToList(LIST);goto build_menu;
        case 'x': //exportCurrentBuild(LIST); WIP
        default: std::cout<<"???"; goto build_menu;
    }
    return 0;
}
